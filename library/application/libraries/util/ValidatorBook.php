<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class ValidatorBook extends CI_Object{
    
    public function validateBook($update = false){
        return true;
        $this->form_validation->set_rules('titulo', 'Titulo do livro', 'required | min_length[2] | max_length[100]');
        $this->form_validation->set_rules('autor', 'Autor do livro', 'required | min_length[2] | max_length[256]');
        $this->form_validation->set_rules('categoria', 'Categoria', 'required | min_lenght[2]');
        
        return $this->form_validation->run();
       
    }
}