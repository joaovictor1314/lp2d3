<?php
include_once APPPATH.'libraries/component/Table.php';
include_once APPPATH.'libraries/util/ButtonGenerator.php';

class UsuarioModel extends CI_Model{

    function __construct() {
        $this->load->library('pessoa');
    }

    public function create(){
        // die(print_r($_POST));
        if(sizeof($_POST) == 0) return;


        if ($this->validator->validateUser()){

            $data = $this->readPostData();
            
            $id = $this->pessoa->create($data);
            
            $fone['numero'] = $this->input->post('telefone');
            $fone['id_pessoa'] = $id;
            $this->load->library('telefone');
            $this->telefone->create($fone);
    
    
            $mail['endereco'] = $this->input->post('email');
            $mail['id_pessoa'] = $id;
            $this->load->library('mail');
            $this->mail->create($mail);
            redirect(base_url('usuario'));
        }
        else{
            return validation_errors();
        }    


        
        
    }


    private function readPostData(){
        $data['nome']= $this->input->post('nome');
        $data['snome']= $this->input->post('snome');
        $pass = $this->input->post('senha');   

        if (strlen($pass) > 0){
            $data['senha']= md5($pass);
        }

        return $data;
    }

    public function listUser(){
        
        $data = $this->pessoa->listaPessoa();
        $url_1 = base_url('usuario/deletar');
        $url = base_url('usuario/editar');
        

        foreach ($data as $key => $row){
        //    $data[$key]['btn'] = ButtonGenerator::editHandler($row);
           $data[$key]['btn'] = ButtonGenerator::editHandler($row, $url).
           ButtonGenerator::deleteHandler($row, $url_1);
           
        }
        

        $label = ['', 'Nome', 'Sobrenome', 'Email', 'Telefone'];
		$table = new Table($data, $label);
		return $table->getHTML();
    }

    public function loadUser($id){
       $pessoa = $this->pessoa->getById($id);

       unset($pessoa['senha']);
       
       $this->load->library('telefone');
       $telefone = $this->telefone->getByUserId($id);
       $v['telefone']=$telefone['numero'];
       
       $this->load->library('mail');
       $email = $this->mail->getByUserId($id);
       $v['email']=$email['endereco'];
       
    //    die(print_r($v));
       $_POST = array_merge($pessoa, $v);

    }

    public function update($id){
        if(sizeof($_POST)==0) return;

        if ($this->validator->validateUser(true)){
            $data = $this->readPostData();
            $this->pessoa->update($data, ['id'=>$id]);


            $this->load->library('telefone');
            $fone['numero'] = $this->input->post('telefone');
            $this->telefone->update($fone, ['id_pessoa' => $id]);

            $this->load->library('mail');
            $mail['endereco'] = $this->input->post('email');
            $this->mail->update($mail, ['id_pessoa' => $id]);

            redirect(base_url('usuario'));
        }else{
            return validation_erros();
        }
    }

    public function delete($id){
        if(sizeof($_POST)==0) return;

            $data = $this->readPostData();
            $this->pessoa->delete($data, ['id'=>$id]);

            $this->load->library('telefone');
            $fone['numero'] = $this->input->post('telefone');
            $this->telefone->delete($fone, ['id_pessoa' => $id]);

            $this->load->library('mail');
            $mail['endereco'] = $this->input->post('email');
            $this->mail->delete($mail, ['id_pessoa' => $id]);

            redirect(base_url('usuario'));
        
    }
    
}