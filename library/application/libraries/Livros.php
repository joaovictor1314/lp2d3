<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/dao/Dao.php';


class Livros extends Dao {

    function __construct(){
        parent::__construct('livros');
    }

   
    
    public function listaBook(){
        $sql = "SELECT id, titulo, categoria, autor from livros";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    
    
}
