<?php
// ini_set('memory_limit', '1024M');
include_once APPPATH.'libraries/component/Table.php';
include_once APPPATH.'libraries/util/ButtonGenerator.php';

class LivrosModel extends CI_Model{

    function __construct() {
        $this->load->library('livros');
    }

    public function create(){

        if(sizeof($_POST) == 0) return;

        $data = $this->readPostData();

        $id = $this->livro->create($data);  
              
    }


    private function readPostData(){
        
        $data['titulo']= $this->input->post('titulo');
        $data['autor']= $this->input->post('autor');
        $data['categoria']= $this->input->post('categoria');
        // print_r($data);
        return $data;
    }



    public function listaBook(){

        $data = $this->livro->listaBook();
        $url_1 = base_url('livros/deletar');
        $url = base_url('livros/editar');
        

        foreach ($data as $key => $row){
           $data[$key]['btn'] = ButtonGenerator::editHandler($row, $url).
           ButtonGenerator::deleteHandler($row, $url_1);
           
        }
        
        $label = ['', 'Titulo', 'Autor', 'Categoria'];
		$table = new Table($data, $label);
		return $table->getHTML();
    }

    public function loadBook($id){
       $livro = $this->livro->getById($id);

       unset($livro['categoria']);
       
       
       $_POST = array_merge($livro, $v);

    }

    public function update($id){
        if(sizeof($_POST)==0) return;

        if ($this->validator->validateBook(true)){
            $data = $this->readPostData();
            $this->livro->update($data, ['id'=>$id]);

            redirect(base_url('livro'));
        }else{
            return validation_erros();
        }
    }

    public function delete($id){
        if(sizeof($_POST)==0) return;

            $data = $this->readPostData();
            $this->livro->delete($data, ['id'=>$id]);

           

            redirect(base_url('livro'));
        
    }
    
}