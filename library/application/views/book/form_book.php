<!-- Default form register -->
<div class="container">

    <div class="row">

        <div class="col-md-6.mx-auto">

        <?php echo form_error('nome', '<div class="alert alert-danger">', '</div>'); ?>

            <form class="text-center border border-light p-5" method="POST">
                <div class="form-row mb-4">
                    <div class="col">
                        <input type="text" id="titulo" name="titulo" value="<?=set_value('titulo')?>" class="form-control" placeholder="titulo">
                    </div>
                    <div class="col">
                        <input type="text" id="autor" name="autor" value="<?=set_value('autor')?>" class="form-control" placeholder="Autor">
                    </div>
                </div>

                <input type="text" id="categoria" name="categoria" value="<?=set_value('categoria')?>" class="form-control mb-4" placeholder="Categoria">


                <button class="btn btn-info my-4 btn-block" type="submit">Entrar</button>
                <a class="btn btn-primary" href="<?= base_url('usuario')?>">Cancelar</a>

            </form>

        </div>

    </div>

</div>


