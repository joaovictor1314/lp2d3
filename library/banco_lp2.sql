-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12-Abr-2022 às 00:55
-- Versão do servidor: 10.1.35-MariaDB
-- versão do PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `conta`
--

CREATE TABLE `conta` (
  `id` int(11) NOT NULL,
  `parceiro` varchar(100) NOT NULL,
  `descricao` varchar(150) NOT NULL,
  `valor` decimal(8,2) NOT NULL,
  `mes` tinyint(4) NOT NULL,
  `ano` smallint(6) NOT NULL,
  `tipo` varchar(8) NOT NULL,
  `liquidada` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `conta`
--

INSERT INTO `conta` (`id`, `parceiro`, `descricao`, `valor`, `mes`, `ano`, `tipo`, `liquidada`, `created_at`) VALUES
(1, 'Magalu', 'Notebook', '2000.00', 1, 2021, 'pagar', 0, '2021-01-26 00:03:52'),
(2, 'Casas Bahia', 'Notebook', '3000.00', 1, 2021, 'pagar', 0, '2021-01-26 00:03:52'),
(3, 'Prefeitura de Sucupira', 'Salário', '3542.18', 1, 2021, 'receber', 0, '2021-01-26 00:03:52'),
(4, 'Aluguel', 'Casa alugada', '680.00', 1, 2021, 'receber', 0, '2021-01-26 00:03:52'),
(5, 'Bandeirante', 'Energia Elétrica', '97.25', 1, 2021, 'pagar', 0, '2021-01-26 00:03:52'),
(6, 'Bandeirante', 'Energia Elétrica', '81.25', 2, 2021, 'pagar', 0, '2021-01-26 00:03:52'),
(7, 'Casas Bahia', 'Primeira conta particionada', '1000.00', 12, 2022, '', 0, '2022-04-05 00:25:27'),
(8, 'Casas Bahia', 'Conta de energia elétrica', '2000.00', 5, 2022, '', 0, '2022-04-05 00:49:59'),
(9, 'Casas Bahia', 'Sofá Cama', '3000.00', 9, 2022, 'pagar', 1, '2022-04-05 01:00:45'),
(10, 'Lojas Cem', 'Sofá cama', '1800.00', 4, 2022, 'pagar', 0, '2022-04-11 22:54:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conta`
--
ALTER TABLE `conta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conta`
--
ALTER TABLE `conta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;