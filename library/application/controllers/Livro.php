<?php 

// ini_set('memory_limit', '-1');
defined('BASEPATH') OR exit('No direct script access allowed');


class Livro extends MY_Controller{

    function __construct() {
        
        parent::__construct();
        $this->load->model('LivrosModel', 'livro');
    }

    public function index()
    {
        $v['table'] = $this->livro->listaBook();
        $html = $this->load->view('book/list_book', $v, true) ;
        $this->show($html);  
    }


    public function cadastro(){
        $this->livro->create();
        $html = $this->load->view('book/form_book', null, true);
        $this->show($html);
    }

    public function editar($id){
        $this->livro->update($id);
        $this->livro->loadlivro($id);
        $html = $this->load->view('book/form_book', null, true);
        $this->show($html); 
    }

    public function deletar($id){
        $this->livro->delete($id);
        $this->livro->loadlivro($id);
        $html = $this->load->view('book/form_book', null, true);
        $this->show($html); 
    }
}