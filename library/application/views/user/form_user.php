<!-- Default form register -->
<div class="container">

    <div class="row">

        <div class="col-md-6.mx-auto">

        <?php echo form_error('nome', '<div class="alert alert-danger">', '</div>'); ?>

            <form class="text-center border border-light p-5" method="POST">
                <div class="form-row mb-4">
                    <div class="col">
                        <input type="text" id="nome" name="nome" value="<?=set_value('nome')?>" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col">
                        <input type="text" id="snome" name="snome" value="<?=set_value('snome')?>" class="form-control" placeholder="Sobrenome">
                    </div>
                </div>

                <input type="email" id="email" name="email" value="<?=set_value('email')?>" class="form-control mb-4" placeholder="E-mail">


                <input type="password" id="senha" name="senha" value="<?=set_value('senha')?>" class="form-control" placeholder="Senha">
                <br/>
                <input type="password" id="conf_senha" name="conf_senha" value="<?=set_value('conf_senha')?>" class="form-control" placeholder="Confirmar a Senha">
                <br/>

                <input type="text" id="telefone" name="telefone" value="<?=set_value('telefone')?>" class="form-control" placeholder="Telefone" aria-describedby="defaultRegisterFormPhoneHelpBlock">
                <br/>


                <button class="btn btn-info my-4 btn-block" type="submit">Entrar</button>
                <a class="btn btn-primary" href="<?= base_url('usuario')?>">Cancelar</a>

            </form>

        </div>

    </div>

</div>


