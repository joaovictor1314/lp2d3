<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/dao/Dao.php';


class Pessoa extends Dao {

    function __construct(){
        parent::__construct('pessoa');
    }

    // public function create($data){
    //     $this->db->insert('pessoa', $data);
    //     return $this->db->insert_id();
    // }
    
    public function listaPessoa(){
        $sql = "SELECT pessoa.id, nome, snome, endereco as email, numero as telefone from pessoa, email, telefone where pessoa.id = telefone.id_pessoa and pessoa.id = email.id_pessoa";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    
    
}
